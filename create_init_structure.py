from pathlib import Path


empty_node = "dir"  # строка для обозачения пустой папки
leaf = "file"  # строка для обозачения файла

def create_tree(root_path: Path, node: dict):
    for key, value in node.items():
        if isinstance(value, dict):
            (root_path / key).mkdir(exist_ok=True)
            create_tree(root_path / key, value)
        else:
            if value == empty_node:
                (root_path / key).mkdir(exist_ok=True)
            else:
                (root_path / key).touch(exist_ok=True)

if __name__ == "__main__":
    root = Path().cwd()

    project_structure = {
                            "data" : {"external": empty_node,
                                    "intermediate": empty_node,
                                    "processed": empty_node,
                                    "raw": empty_node
                                    },
                            "docs": empty_node,
                            "models": empty_node,
                            "notebooks": empty_node,
                            "references": empty_node,
                            "requirements.txt": leaf,
                            "src": {
                                    "data": {
                                                "make_dataset.py": leaf
                                            },
                                    "features": {
                                                    "build_features.py": leaf
                                                },
                                    "models": {
                                                    "train_model.py": leaf
                                                }
                                }
                        }   

    create_tree(root, project_structure)